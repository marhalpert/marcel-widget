// Input
// ===========

import React from 'react'
import style from './index.scss'

export default class Input extends React.Component {
  render() {
    return <div className={style.inputContaine}>
      <input className={style.input}
        type={this.props.type} id={this.props.id}
        required={this.props.required} autoComplete={this.props.autoComplete}/>
      <label className={style.label} htmlFor={this.props.id}>
        {this.props.labelText}
      </label>
      {this.props.showBar && <div className={style.bar}></div>}
    </div>
  }
}

Input.propTypes = {
  autoComplete: React.PropTypes.string,
  id: React.PropTypes.string.isRequired,
  labelText: React.PropTypes.string.isRequired,
  required: React.PropTypes.string,
  showBar: React.PropTypes.bool,
  type: React.PropTypes.string.isRequired
}
