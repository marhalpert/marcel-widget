// Time
// ===========

import React from 'react'
import moment from 'moment'
import style from './index.scss'

export default class Time extends React.Component {
  constructor(options = {}) {
    super(options)
    this.setTime = this.setTime.bind(this)
    this.setInterval = this.setInterval.bind(this)
  }

  setTime() {
    this.setState({
      time: moment().format('hh:mm'),
      period: moment().format('A'),
      date: moment().format('ddd, MMM YYYY')
    })
  }

  setInterval() {
    window.setInterval(function () {
      this.setTime()
    }.bind(this), 60000)
  }

  componentWillMount() {
    this.setTime()
  }

  componentDidMount() {
    this.setInterval()
  }

  componentWillUnmount() {
    window.clearInterval(this.setInterval)
  }

  render() {
    return <div className={style.Time}>
        <time>
          <span className={style.Clock}>
            {this.state.time}
          </span>
          <sub className={style.Period}> {this.state.period}
          </sub><br />
          <span className={style.Period}>{this.state.date}</span>
        </time>
      </div>
  }
}
