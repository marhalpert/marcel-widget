// Colors list
// ===========

import React from 'react'
import style from './index.scss'

export default class Message extends React.Component {
  render() {
    return <div className={style.message}>
      {this.props.message}
    </div>
  }
}

Message.propTypes = {
  message: React.PropTypes.string
}
