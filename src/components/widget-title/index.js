// Widget Title
// ===========

import React from 'react'
import style from './index.scss'

export default class WidgetTitle extends React.Component {
  render() {
    return <h1 className={style.WidgetTitle}>
      {this.props.text}
      </h1>
  }
}

WidgetTitle.propTypes = {
  text: React.PropTypes.string
}
