// Colors list
// ===========

import React from 'react'
import Icon from '../icon/'
import style from './index.scss'

export default class IconButton extends React.Component {
  render() {
    return <button className={style.iconButton} onClick={this.props.action}>
      <Icon IconClass={this.props.icon} />
    </button>
  }
}

IconButton.propTypes = {
  action: React.PropTypes.func,
  icon: React.PropTypes.string
}
