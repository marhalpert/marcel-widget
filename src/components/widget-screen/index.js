// Widget screen
// ==============

import React from 'react'
import classNames from 'classnames/bind'
import EventBus from '../../utils/event-bus'
import BackBody from '../back-body/'
import FrontBody from '../front-body/'
import IconButton from '../icon-button/'
import WidgetTitle from '../widget-title/'
import style from './index.scss'

let cx = classNames.bind(style)
let mainWraper = document.querySelector('.main-wrapper')
let configuration = {
  title: mainWraper.dataset.title,
  units: mainWraper.dataset.units
}

export default class WidgetScreen extends React.Component {
  constructor(options = {}) {
    super(options)
    this.state = {
      flipped: false,
      title: configuration.title,
      units: configuration.units
    }
    this.flip = this.flip.bind(this)
    this.close = this.close.bind(this)
    this.getHeader = this.getHeader.bind(this)
    this.getWind = this.getWind.bind(this)
    this.openEmbed = this.openEmbed.bind(this)
  }

  flip() {
    if (!this.state.flipped) this.setState({ flipped: true })
  }

  close() {
    if (this.state.flipped) this.setState({ flipped: false })
  }

  getHeader() {
    if (!this.state.title) {
      this.setState({title: 'Weathe Widget'})
    }
  }

  getUnits() {
    if (!this.state.units) {
      this.setState({units: 'metric'})
    }
  }

  getWind() {
    if (this.state.showWind === false) {
      this.setState({
        showWindText: 'Hide Wind',
        showWind: true
      })
    } else {
      this.setState({
        showWindText: 'Show Wind',
        showWind: false
      })
    }
  }

  openEmbed() {
    if (this.state.showEmbed === false) {
      this.setState({
        embedText: 'Close',
        showEmbed: true
      })
    } else {
      this.setState({
        embedText: 'Embed Widget',
        showEmbed: false
      })
    }
  }

  getConfig() {
    this.getHeader()
    this.getUnits()
  }

  weatherError() {
    this.setState({weatherError: true})
  }

  componentWillMount() {
    this.getConfig()
    this.getWind()
    this.openEmbed()
  }

  componentDidMount() {
    EventBus.subscribe('weather:Error', () => {
      this.weatherError()
    })
  }

  componentWillUnmount() {
    EventBus.unsubscribe('weather:Error', () => {
      this.weatherError()
    })
  }

  render() {
    let flipper = cx({
      widget: true,
      widgetFlipped: this.state.flipped
    })
    return <div className={style.widgetWrapper}>
      <div className={flipper}>
        <div className={style.widgetFront}>
          <header className={style.widgetHeader}>
            <WidgetTitle text={this.state.title} />
            <IconButton icon={'icon-cog'} action={this.flip} />
          </header>
          <FrontBody showWind={this.state.showWind} units={this.state.units} />
          <footer className={style.widgetFooter}>
            <button onClick={this.getWind} className={style.footerButton}>
              {this.state.showWindText}
            </button>
          </footer>
        </div>
        <div className={style.widgetBack}>
          <header className={style.widgetHeader}>
            <WidgetTitle text={'Personalize your widget'} />
            <IconButton icon={'icon-plus'} action={this.close} />
          </header>
          <BackBody showEmbed={this.state.showEmbed} />
          <footer className={style.widgetFooter}>
            <button onClick={this.openEmbed} className={style.footerButton}>
              {this.state.embedText}
            </button>
          </footer>
        </div>
      </div>
    </div>
  }
}

WidgetScreen.propTypes = {
  params: React.PropTypes.object,
  routeParams: React.PropTypes.object,
  history: React.PropTypes.object.isRequired
}
