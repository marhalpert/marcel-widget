// Colors list
// ===========

import React from 'react'
import classNames from 'classnames/bind'
import Input from '../input/'
import style from './index.scss'

let cx = classNames.bind(style)

let title = 'Your Title'
let units = 'Temperature units'

export default class BackBody extends React.Component {
  render() {
    let embedStyles = cx({
      embedSection: true,
      showEmbedSection: this.props.showEmbed
    })
    return <div className={style.WidgetBody}>
        <Input type="text"
          id="title" labelText="Widget Title" showBar autoComplete="off"
          required="required" />
        <Input type="checkbox"
          id="units" labelText="Metric System" required="required" />
        <div className={embedStyles}>
          <h3>Please add this code before the
            closing tag of your HTML body element.
          </h3>
          <pre className={style.pre}>
            <code className={style.code}>
              &lt;main
              <br />  class="main-wrapper"
              <br />  data-title="{title}"
              <br />  data-units="{units}"&gt; //imperial or metric
              <br />&lt;/main&gt;
              <br />
              &lt;script src="some-path-to-this-file/main.js"&gt;&lt;/script&gt;
            </code>
          </pre>
        </div>
      </div>
  }
}

BackBody.propTypes = {
  showEmbed: React.PropTypes.bool
}
