// Weather
// ===========

import React from 'react'
import classNames from 'classnames/bind'
import { geoLocation } from '../../utils/geoLocation/'
import { weather } from '../../utils/weather/'
import EventBus from '../../utils/event-bus'
import Icon from '../icon/'
import jsonIcons from '../weatherIcons/weatherIcons.json'
import Message from '../message/'
import style from './index.scss'

// AIzaSyD-4meRuYNL_GJquWCvcJ9TOLZLUzGVJxo

let cx = classNames.bind(style)

export default class Weather extends React.Component {
  constructor(options = {}) {
    super(options)
    let props = this.props.config.units
    this.state = {
      name: false,
      units: props
    }
  }

  haveWeather(weatherData) {
    this.setState({
      name: weatherData.name,
      temp: weatherData.main.temp.toFixed(0),
      description: weatherData.weather[0].description,
      windSpeed: weatherData.wind.speed,
      windDirection: weatherData.wind.deg,
      id: weatherData.weather[0].id
    })
    this.getIcon(weatherData.weather[0].id)
  }

  getIcon(iconId) {
    // Based on:
    // https://gist.github.com/tbranyen/62d974681dea8ee0caa1
    let prefix = 'wi wi-'
    let code = iconId
    let icon = jsonIcons[code].icon
    if (!(code > 699 && code < 800) && !(code > 899 && code < 1000)) {
      icon = 'day-' + icon
    }
    let prefixedIcon = icon = prefix + icon
    this.setState({
      iconClass: prefixedIcon
    })
  }

  couldNotLocate(err) {
    let message = 'Sorry we need your location to proceed'
    if (err.code === 1) {
      this.setState({
        located: false,
        message: message
      })
    }
  }

  couldNotGetWeather(err) {
    let message = 'Opps... Sorry'
    console.log(err)
    this.setState({
      message: message
    })
  }

  haveLocation(coordinates) {
    this.setState({
      coordinates
    })
  }

  getGeoLocation() {
    geoLocation()
    .then((coordinates) => {
      this.haveLocation(coordinates)
      this.getWeather()
    }).catch(err => {
      this.couldNotLocate(err)
    })
  }

  getWeather() {
    weather(this.state.coordinates, this.state.units)
    .then((weather) => {
      this.haveWeather(weather.data)
      EventBus.broadcast('weather:Loaded')
    }).catch(err => {
      EventBus.broadcast('weather:Error')
      this.couldNotGetWeather(err)
    })
  }

  componentDidMount() {
    this.getGeoLocation()
  }

  render() {
    let loadding = cx({
      weather: true,
      weatherDataLoaded: this.state.name,
      weatherDataNotLoaded: this.state.message,
      showWind: this.props.config.showWind
    })
    let weatherIcon = cx({
      sup: true,
      wi: true,
      'wi-celsius': this.state.units === 'metric',
      'wi-fahrenheit': this.state.units === 'imperial'
    })
    let windIcon = cx({
      wi: true,
      'wi-wind': this.state.units
    })
    let direction = this.state.windDirection
    let rotateIcon = {
      transform: `rotate(${direction}deg)`,
      display: 'inline-block'
    }

    return <div className={loadding}>
      <div className={style.innerWeather}>
        <Message message={this.state.message} />
        <div className={style.iconWeapper}>
          <Icon IconClass={this.state.iconClass} />
        </div>
        <div className={style.infoWrapper}>
          <div className={style.temp}>
            {this.state.temp}
            <sup className={weatherIcon}></sup>
          </div>
          <div className={style.name}>{this.state.name}</div>
          <div className={style.windWrapper}>
            <div style={rotateIcon}>
              <Icon IconClass={windIcon} />
            </div> {this.state.windSpeed} km/h
          </div>
        </div>
      </div>
    </div>
  }
}

Weather.propTypes = {
  config: React.PropTypes.object
}
