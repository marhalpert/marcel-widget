// Colors list
// ===========

import React from 'react'

export default class Icon extends React.Component {
  render() {
    return <i className={this.props.IconClass}></i>
  }
}

Icon.propTypes = {
  IconClass: React.PropTypes.string
}
