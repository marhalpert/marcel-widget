// Colors list
// ===========

import React from 'react'
import Time from '../time/'
import Weather from '../weather/'
import style from './index.scss'

export default class FrontBody extends React.Component {
  render() {
    return <div className={style.WidgetBody}>
        <Weather config={this.props} />
        <Time/>
      </div>
  }
}
