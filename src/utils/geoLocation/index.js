// GeoLocation
// ===========

export function geoLocation() {
  return new Promise(function(resolve, reject) {
    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        (location) => {
          resolve({
            lat: location.coords.latitude,
            lon: location.coords.longitude
          })
        },
        (error) => {
          reject(error)
        })
    }
  })
}
