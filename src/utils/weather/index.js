// weather
// ===========
import axios from 'axios'

export function weather(coordinates, units) {
  return new Promise(function(resolve, reject) {
    axios.get('http://api.openweathermap.org/data/2.5/weather', {
      params: {
        units: units,
        lat: coordinates.lat,
        lon: coordinates.lon,
        appid: '1b29e55ba1f3079811348994832eddcb'
      }
    })
    .then((response) => {
      resolve(response)
    })
    .catch(err => {
      reject(err)
    })
  })
}
