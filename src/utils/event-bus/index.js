// Global event bus
// ================

import { Channel } from 'airwaves'

var EventBus = new Channel()

export default EventBus
