// Client routes
// =============

import React from 'react'
import { Route } from 'react-router'
import App from './components/app'
import WidgetScreen from './components/widget-screen'

export default <Route component={App}>
  <Route path="/(:name)" component={WidgetScreen} />
</Route>
