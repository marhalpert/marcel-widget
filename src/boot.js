// Client boot file
// ================

import './stylesheets/reset.css'
import './stylesheets/top-wrappers.css'
import './stylesheets/icons.css'
import './stylesheets/google-fonts.css'
import 'babel/polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import Router from 'react-router'
import { createHashHistory } from 'history'
import routes from './routes'

const history = createHashHistory({ queryKey: false })

ReactDOM.render(
  <Router children={routes} history={history} />,
  document.querySelector('.main-wrapper')
)
