# widget preview

Copy `.env.sample` to `.env`

`$ yarn install` or `$ npm install`

Then `$ npm start`

navigate to `localhost:9000`

# Embed

Please build the widget `$ npm run build`

After building

The customize screen is now finished so to embed please edit the `index.html`

Change the `data-title="My Weather Widget"` 

`data-units="metric"` The units will accept `metric` or `imperial` as parameters.

navigate to the `index.html` on any browser